# -*- coding: utf-8 -*-

from functools import wraps
from sqlalchemy import asc, desc
from flask import Blueprint, redirect, url_for, current_app, request, flash, Response
from flask_login import current_user, login_user, logout_user
from flask_babelplus import gettext as _
from flask_allows import Permission, And
from flaskbb.extensions import db, allows, csrf
from flaskbb.utils.settings import flaskbb_config
from flaskbb.utils.helpers import (
    get_online_users, time_diff, format_quote, render_template, do_topic_action
)

from flaskbb.utils.requirements import (
    CanAccessForum,
    CanAccessTopic,
    CanDeletePost,
    CanDeleteTopic,
    CanEditPost,
    CanPostReply,
    CanPostTopic,
    IsAtleastModeratorInForum,
)
from flaskbb.forum.models import (
    Category, Forum, Topic, Post, ForumsRead, TopicsRead
)
from flaskbb.forum.forms import (
    NewTopicForm,
    QuickreplyForm,
    ReplyForm,
    ReportForm,
    SearchPageForm,
    UserSearchForm,
)

from flaskbb.user.models import User

import datetime
import json

alfamas = Blueprint(
    "alfamas", 
    __name__, 
    #subdomain="foros",
    template_folder="templates",
    static_folder='static'
)

RESIDENTES_URL = "http://residentes.naifo.org"

@alfamas.context_processor
def inject_residentes_url():
    return dict(residentes_url=RESIDENTES_URL)

def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if current_user is not None and current_user.is_authenticated:
            return f(*args, **kwargs)

        else:
            return redirect(url_for("alfamas.index"))

    return decorated

@alfamas.route("/")
def index():
    return redirect(RESIDENTES_URL)

@alfamas.route("/<forum>/login/<username>/<password>")
def login(forum, username, password):

    user, authenticated = User.authenticate(username, password)

    if user and authenticated:
        login_user(user)

    return redirect('/alfamas/%s' % forum)
    
@alfamas.route("/logout")
@login_required
def logout():
    logout_user()
    print "logout"
    return redirect(RESIDENTES_URL+"/logout")

@csrf.exempt
@alfamas.route("/register", methods=['PUT'])
def register():

    username = request.form['username']
    password = request.form['password']
    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']

    if User.query.filter_by(username=username).first():
        return "OK"

    if User.query.filter_by(email=email).first():
        return "OK"
    
    db.session.close()

    user = User(
        username=username,
        email=email,
        password=password,
        date_joined=datetime.datetime.utcnow(),
        primary_group_id=4,
        notes=firstname + "<br>" + lastname,
        language='es'
    )
    
    user.save()

    #print "User created", firstname, lastname

    return "OK"

@csrf.exempt
@alfamas.route("/reset_password", methods=['PUT'])
def reset_password():

    username = request.form['username']
    password = request.form['password']

    user = User.query.filter_by(username=username).first()

    #db.session.close()

    user.password = password    
    user.save()

    return "OK"

@alfamas.route("/<int:forum_id>")
@alfamas.route("/<int:forum_id>-<slug>")
@login_required
#@allows.requires(CanAccessForum())
def view_forum(forum_id, slug=None):
    page = request.args.get('page', 1, type=int)

    forum_instance, forumsread = Forum.get_forum(
        forum_id=forum_id, user=current_user
    )

    if forum_instance.external:
        return redirect(forum_instance.external)

    topics = Forum.get_topics(
        forum_id=forum_instance.id, user=current_user, page=page,
        per_page=flaskbb_config["TOPICS_PER_PAGE"]
    )

    return render_template(
        "m_forum.html", forum=forum_instance,
        topics=topics, forumsread=forumsread,
    )

@alfamas.route("/topic/<int:topic_id>", methods=["POST", "GET"])
@alfamas.route("/topic/<int:topic_id>-<slug>", methods=["POST", "GET"])
@allows.requires(CanAccessTopic())
def view_topic(topic_id, slug=None):
    page = request.args.get('page', 1, type=int)

    # Fetch some information about the topic
    topic = Topic.get_topic(topic_id=topic_id, user=current_user)

    # Count the topic views
    topic.views += 1
    topic.save()

    # fetch the posts in the topic
    posts = Post.query.\
        join(User, Post.user_id == User.id).\
        filter(Post.topic_id == topic.id).\
        add_entity(User).\
        order_by(Post.id.asc()).\
        paginate(page, flaskbb_config['POSTS_PER_PAGE'], False)

    # Update the topicsread status if the user hasn't read it
    forumsread = None
    if current_user.is_authenticated:
        forumsread = ForumsRead.query.\
            filter_by(user_id=current_user.id,
                      forum_id=topic.forum.id).first()

    topic.update_read(current_user, topic.forum, forumsread)

    form = None
    if Permission(CanPostReply):
        form = QuickreplyForm()
        if form.validate_on_submit():
            post = form.save(current_user, topic)
            return view_post(post.id)

    return render_template("m_topic.html", topic=topic, posts=posts,
                           last_seen=time_diff(), form=form)

@alfamas.route("/post/<int:post_id>")
@login_required
def view_post(post_id):
    post = Post.query.filter_by(id=post_id).first_or_404()
    count = post.topic.post_count
    page = count / flaskbb_config["POSTS_PER_PAGE"]

    if count > flaskbb_config["POSTS_PER_PAGE"]:
        page += 1
    else:
        page = 1

    return redirect('/alfamas'+post.topic.url + "?page=%d#pid%s" % (page, post.id))


@alfamas.route("/<int:forum_id>/topic/new", methods=["POST", "GET"])
@alfamas.route("/<int:forum_id>-<slug>/topic/new", methods=["POST", "GET"])
@login_required
def new_topic(forum_id, slug=None):
    forum_instance = Forum.query.filter_by(id=forum_id).first_or_404()

    print "new_topic"
    if not Permission(CanPostTopic):
        flash(_("You do not have the permissions to create a new topic."),
              "danger")
        return redirect('/alfamas'+forum_instance.url)

    print "NewTopicForm"
    form = NewTopicForm()
    if request.method == "POST":
        if "preview" in request.form and form.validate():
            print "preview"
            return render_template(
                "m_new_topic.html", forum=forum_instance,
                form=form, preview=form.content.data
            )
        if "submit" in request.form and form.validate():
            print "submit"
            topic = form.save(current_user, forum_instance)
            # redirect to the new topic
            return redirect(url_for('alfamas.view_topic', topic_id=topic.id))

    print "render"
    return render_template(
        "m_new_topic.html", forum=forum_instance, form=form
    )


@alfamas.route("/topic/<int:topic_id>/delete", methods=["POST"])
@alfamas.route("/topic/<int:topic_id>-<slug>/delete", methods=["POST"])
@login_required
def delete_topic(topic_id=None, slug=None):
    print "delete_topic"
    topic = Topic.query.filter_by(id=topic_id).first_or_404()

    if not Permission(CanDeleteTopic):
        print "no_permission"
        flash(_("You do not have the permissions to delete this topic."),
              "danger")
        return redirect('/alfamas'+topic.forum.url)

    print "cant delete"
    involved_users = User.query.filter(Post.topic_id == topic.id,
                                       User.id == Post.user_id).all()
    topic.delete(users=involved_users)
    print "delete and redirect to "
    print redirect(url_for("alfamas.view_forum", forum_id=topic.forum_id))
    return redirect(url_for("alfamas.view_forum", forum_id=topic.forum_id))


@alfamas.route("/topic/<int:topic_id>/lock", methods=["POST"])
@alfamas.route("/topic/<int:topic_id>-<slug>/lock", methods=["POST"])
@login_required
def lock_topic(topic_id=None, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()

    if not Permission(IsAtleastModeratorInForum(forum=topic.forum)):
        flash(_("You do not have the permissions to lock this topic."),
              "danger")
        return redirect(topic.url)

    topic.locked = True
    topic.save()
    return redirect(topic.url)


@alfamas.route("/topic/<int:topic_id>/unlock", methods=["POST"])
@alfamas.route("/topic/<int:topic_id>-<slug>/unlock", methods=["POST"])
@login_required
def unlock_topic(topic_id=None, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()

    if not Permission(IsAtleastModeratorInForum(forum=topic.forum)):
        flash(_("You do not have the permissions to unlock this topic."),
              "danger")
        return redirect(topic.url)

    topic.locked = False
    topic.save()
    return redirect(topic.url)


@alfamas.route("/topic/<int:topic_id>/highlight", methods=["POST"])
@alfamas.route("/topic/<int:topic_id>-<slug>/highlight", methods=["POST"])
@login_required
def highlight_topic(topic_id=None, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()

    if not Permission(IsAtleastModeratorInForum(forum=topic.forum)):
        flash(_("You do not have the permissions to highlight this topic."),
              "danger")
        return redirect(topic.url)

    topic.important = True
    topic.save()
    return redirect(topic.url)


@alfamas.route("/topic/<int:topic_id>/trivialize", methods=["POST"])
@alfamas.route("/topic/<int:topic_id>-<slug>/trivialize", methods=["POST"])
@login_required
def trivialize_topic(topic_id=None, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()

    # Unlock is basically the same as lock
    if not Permission(IsAtleastModeratorInForum(forum=topic.forum)):
        flash(_("You do not have the permissions to trivialize this topic."),
              "danger")
        return redirect(topic.url)

    topic.important = False
    topic.save()
    return redirect(topic.url)


@alfamas.route("/<int:forum_id>/edit", methods=["POST", "GET"])
@alfamas.route("/<int:forum_id>-<slug>/edit", methods=["POST", "GET"])
@login_required
def manage_forum(forum_id, slug=None):
    page = request.args.get('page', 1, type=int)

    forum_instance, forumsread = Forum.get_forum(forum_id=forum_id,
                                                 user=current_user)

    # remove the current forum from the select field (move).
    available_forums = Forum.query.order_by(Forum.position).all()
    available_forums.remove(forum_instance)

    if not Permission(IsAtleastModeratorInForum(forum=forum_instance)):
        flash(_("You do not have the permissions to moderate this forum."),
              "danger")
        return redirect(forum_instance.url)

    if forum_instance.external:
        return redirect(forum_instance.external)

    topics = Forum.get_topics(
        forum_id=forum_instance.id, user=current_user, page=page,
        per_page=flaskbb_config["TOPICS_PER_PAGE"]
    )

    mod_forum_url = url_for("forum.manage_forum", forum_id=forum_instance.id,
                            slug=forum_instance.slug)

    # the code is kind of the same here but it somehow still looks cleaner than
    # doin some magic
    if request.method == "POST":
        ids = request.form.getlist("rowid")
        tmp_topics = Topic.query.filter(Topic.id.in_(ids)).all()

        # locking/unlocking
        if "lock" in request.form:
            changed = do_topic_action(topics=tmp_topics, user=current_user,
                                      action="locked", reverse=False)

            flash(_("%(count)s Topics locked.", count=changed), "success")
            return redirect(mod_forum_url)

        elif "unlock" in request.form:
            changed = do_topic_action(topics=tmp_topics, user=current_user,
                                      action="locked", reverse=True)
            flash(_("%(count)s Topics unlocked.", count=changed), "success")
            return redirect(mod_forum_url)

        # highlighting/trivializing
        elif "highlight" in request.form:
            changed = do_topic_action(topics=tmp_topics, user=current_user,
                                      action="important", reverse=False)
            flash(_("%(count)s Topics highlighted.", count=changed), "success")
            return redirect(mod_forum_url)

        elif "trivialize" in request.form:
            changed = do_topic_action(topics=tmp_topics, user=current_user,
                                      action="important", reverse=True)
            flash(_("%(count)s Topics trivialized.", count=changed), "success")
            return redirect(mod_forum_url)

        # deleting
        elif "delete" in request.form:
            changed = do_topic_action(topics=tmp_topics, user=current_user,
                                      action="delete", reverse=False)
            flash(_("%(count)s Topics deleted.", count=changed), "success")
            return redirect(mod_forum_url)

        # moving
        elif "move" in request.form:
            new_forum_id = request.form.get("forum")

            if not new_forum_id:
                flash(_("Please choose a new forum for the topics."), "info")
                return redirect(mod_forum_url)

            new_forum = Forum.query.filter_by(id=new_forum_id).first_or_404()
            # check the permission in the current forum and in the new forum

            if not Permission(
                And(
                    IsAtleastModeratorInForum(forum_id=new_forum_id),
                    IsAtleastModeratorInForum(forum=forum_instance)
                )
            ):
                flash(_("You do not have the permissions to move this topic."),
                      "danger")
                return redirect(mod_forum_url)

            new_forum.move_topics_to(tmp_topics)
            return redirect(mod_forum_url)

    return render_template(
        "edit_forum.html", forum=forum_instance, topics=topics,
        available_forums=available_forums, forumsread=forumsread,
    )


@alfamas.route("/topic/<int:topic_id>/post/new", methods=["POST", "GET"])
@alfamas.route("/topic/<int:topic_id>-<slug>/post/new", methods=["POST", "GET"])
@login_required
def new_post(topic_id, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()

    if not Permission(CanPostReply):
        flash(_("You do not have the permissions to post in this topic."),
              "danger")
        print "first", topic.forum.url
        return redirect(topic.forum.url)

    form = ReplyForm()
    if form.validate_on_submit():
        if "preview" in request.form:
            return render_template(
                "m_new_post.html", topic=topic,
                form=form, preview=form.content.data
            )
        else:
            post = form.save(current_user, topic)
            return view_post(post.id)

    return render_template("m_new_post.html", topic=topic, form=form)


@alfamas.route(
    "/topic/<int:topic_id>/post/<int:post_id>/reply", methods=["POST", "GET"]
)
@login_required
def reply_post(topic_id, post_id):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()
    post = Post.query.filter_by(id=post_id).first_or_404()

    if not Permission(CanPostReply):
        flash(_("You do not have the permissions to post in this topic."),
              "danger")
        return redirect(topic.forum.url)

    form = ReplyForm()
    if form.validate_on_submit():
        if "preview" in request.form:
            return render_template(
                "m_new_post.html", topic=topic,
                form=form, preview=form.content.data
            )
        else:
            form.save(current_user, topic)
            return redirect('/alfamas'+post.topic.url)
    else:
        form.content.data = format_quote(post.username, post.content)

    return render_template("m_new_post.html", topic=post.topic, form=form)


@alfamas.route("/post/<int:post_id>/edit", methods=["POST", "GET"])
@login_required
def edit_post(post_id):
    post = Post.query.filter_by(id=post_id).first_or_404()

    if not Permission(CanEditPost):
        flash(_("You do not have the permissions to edit this post."),
              "danger")
        return redirect('/alfamas'+post.topic.url)

    form = ReplyForm()
    if form.validate_on_submit():
        if "preview" in request.form:
            return render_template(
                "m_new_post.html", topic=post.topic,
                form=form, preview=form.content.data
            )
        else:
            form.populate_obj(post)
            post.date_modified = datetime.datetime.utcnow()
            post.modified_by = current_user.username
            post.save()
            return redirect('/alfamas'+post.topic.url)
    else:
        form.content.data = post.content

    return render_template("m_new_post.html", topic=post.topic, form=form)


@alfamas.route("/post/<int:post_id>/delete", methods=["POST"])
@login_required
def delete_post(post_id):
    post = Post.query.filter_by(id=post_id).first_or_404()

    # TODO: Bulk delete

    if not Permission(CanDeletePost):
        flash(_("You do not have the permissions to delete this post."),
              "danger")
        return redirect('/alfamas'+post.topic.url)

    first_post = post.first_post
    topic_url = '/alfamas'+post.topic.url
    forum_url = '/alfamas'+post.topic.forum.url

    post.delete()

    # If the post was the first post in the topic, redirect to the forums
    if first_post:
        return redirect(forum_url)
    return redirect(topic_url)


















@alfamas.route("/post/<int:post_id>/report", methods=["GET", "POST"])
@login_required
def report_post(post_id):
    post = Post.query.filter_by(id=post_id).first_or_404()

    form = ReportForm()
    if form.validate_on_submit():
        form.save(current_user, post)
        flash(_("Thanks for reporting."), "success")

    return render_template("report_post.html", form=form)


@alfamas.route("/post/<int:post_id>/raw", methods=["POST", "GET"])
@login_required
def raw_post(post_id):
    post = Post.query.filter_by(id=post_id).first_or_404()
    return format_quote(username=post.username, content=post.content)


@alfamas.route("/<int:forum_id>/markread", methods=["POST"])
@alfamas.route("/<int:forum_id>-<slug>/markread", methods=["POST"])
@login_required
def markread(forum_id=None, slug=None):
    # Mark a single forum as read
    if forum_id:
        forum_instance = Forum.query.filter_by(id=forum_id).first_or_404()
        forumsread = ForumsRead.query.filter_by(
            user_id=current_user.id, forum_id=forum_instance.id
        ).first()
        TopicsRead.query.filter_by(user_id=current_user.id,
                                   forum_id=forum_instance.id).delete()

        if not forumsread:
            forumsread = ForumsRead()
            forumsread.user_id = current_user.id
            forumsread.forum_id = forum_instance.id

        forumsread.last_read = datetime.datetime.utcnow()
        forumsread.cleared = datetime.datetime.utcnow()

        db.session.add(forumsread)
        db.session.commit()

        flash(_("Forum %(forum)s marked as read.", forum=forum_instance.title),
              "success")

        return redirect(forum_instance.url)

    # Mark all forums as read
    ForumsRead.query.filter_by(user_id=current_user.id).delete()
    TopicsRead.query.filter_by(user_id=current_user.id).delete()

    forums = Forum.query.all()
    forumsread_list = []
    for forum_instance in forums:
        forumsread = ForumsRead()
        forumsread.user_id = current_user.id
        forumsread.forum_id = forum_instance.id
        forumsread.last_read = datetime.datetime.utcnow()
        forumsread.cleared = datetime.datetime.utcnow()
        forumsread_list.append(forumsread)

    db.session.add_all(forumsread_list)
    db.session.commit()

    flash(_("All forums marked as read."), "success")

    return redirect(url_for("forum.index"))


@alfamas.route("/who-is-online")
def who_is_online():
    if current_app.config['REDIS_ENABLED']:
        online_users = get_online_users()
    else:
        online_users = User.query.filter(User.lastseen >= time_diff()).all()
    return render_template("online_users.html",
                           online_users=online_users)


@alfamas.route("/memberlist", methods=['GET', 'POST'])
def memberlist():
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', 'reg_date')
    order_by = request.args.get('order_by', 'asc')

    sort_obj = None
    order_func = None
    if order_by == 'asc':
        order_func = asc
    else:
        order_func = desc

    if sort_by == 'reg_date':
        sort_obj = User.id
    elif sort_by == 'post_count':
        sort_obj = User.post_count
    else:
        sort_obj = User.username

    search_form = UserSearchForm()
    if search_form.validate():
        users = search_form.get_results().\
            paginate(page, flaskbb_config['USERS_PER_PAGE'], False)
        return render_template("memberlist.html", users=users,
                               search_form=search_form)
    else:
        users = User.query.order_by(order_func(sort_obj)).\
            paginate(page, flaskbb_config['USERS_PER_PAGE'], False)
        return render_template("memberlist.html", users=users,
                               search_form=search_form)


@alfamas.route("/topictracker")
@login_required
def topictracker():
    page = request.args.get("page", 1, type=int)
    topics = current_user.tracked_topics.\
        outerjoin(TopicsRead,
                  db.and_(TopicsRead.topic_id == Topic.id,
                          TopicsRead.user_id == current_user.id)).\
        add_entity(TopicsRead).\
        order_by(Topic.last_updated.desc()).\
        paginate(page, flaskbb_config['TOPICS_PER_PAGE'], True)

    return render_template("topictracker.html", topics=topics)


@alfamas.route("/topictracker/<int:topic_id>/add", methods=["POST"])
@alfamas.route("/topictracker/<int:topic_id>-<slug>/add", methods=["POST"])
@login_required
def track_topic(topic_id, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()
    current_user.track_topic(topic)
    current_user.save()
    return redirect(topic.url)


@alfamas.route("/topictracker/<int:topic_id>/delete", methods=["POST"])
@alfamas.route("/topictracker/<int:topic_id>-<slug>/delete", methods=["POST"])
@login_required
def untrack_topic(topic_id, slug=None):
    topic = Topic.query.filter_by(id=topic_id).first_or_404()
    current_user.untrack_topic(topic)
    current_user.save()
    return redirect(topic.url)


@alfamas.route("/search", methods=['GET', 'POST'])
@login_required
def search():
    form = SearchPageForm()

    if form.validate_on_submit():
        result = form.get_results()
        return render_template('search_result.html', form=form,
                               result=result)

    return render_template('search_form.html', form=form)
